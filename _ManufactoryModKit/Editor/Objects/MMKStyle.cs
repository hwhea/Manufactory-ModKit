﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMKStyle {

    public static GUIStyle TextColor(Color col)
    {
        GUIStyle style = new GUIStyle();
        style.normal.textColor = col;
        return style;
    }

}
