﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManufactoryEntity {

    public enum EntityType
    {
        Item,
        Machine
    }

    public string entityID;
    public string entityName;
    public string entityDesc;
    public string thumbnail;
    public float price;
    public string gameObjectPath;
    public EntityType entityType;

    public ManufactoryEntity(string entityID, string entityName, string entityDescription, float price, string gameObjectPath, EntityType entityType)
    {
        this.entityID = entityID;
        this.entityName = entityName;
        this.entityDesc = entityDescription;
        this.price = price;
        this.gameObjectPath = gameObjectPath;
        this.entityType = entityType;
    }

    public ManufactoryEntity(ManufactoryEntity ma)
    {
        this.entityID = ma.entityID;
        this.entityName = ma.entityName;
        this.entityDesc = ma.entityDesc;
        this.price = ma.price;
        this.gameObjectPath = ma.gameObjectPath;
        this.entityType = ma.entityType;
    }

    public static bool operator ==(ManufactoryEntity ma1, ManufactoryEntity ma2)
    {
        if (ma1.entityDesc != ma2.entityDesc)
            return false;

        if (ma1.entityID != ma2.entityID)
            return false;

        if (ma1.entityName != ma2.entityName)
            return false;

        if (ma1.price != ma2.price)
            return false;

        if (ma1.gameObjectPath != ma2.gameObjectPath)
            return false;

        if (ma1.entityType != ma2.entityType)
            return false;

        return true;
    }

    public static bool operator !=(ManufactoryEntity ma1, ManufactoryEntity ma2)
    {
        if (ma1.entityDesc != ma2.entityDesc)
            return true;

        if (ma1.entityID != ma2.entityID)
            return true;

        if (ma1.entityName != ma2.entityName)
            return true;

        if (ma1.price != ma2.price)
            return true;

        if (ma1.gameObjectPath != ma2.gameObjectPath)
            return true;

        if (ma1.entityType != ma2.entityType)
            return true;

        return false;
    }


    public ManufactoryEntity() { }
}
