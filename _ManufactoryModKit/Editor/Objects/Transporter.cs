﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transporter {

    public enum Type
    {
        Conveyor
    }

    public List<GameObject> variants = new List<GameObject>();

    public Type type;

    public Transporter(Type type, List<GameObject> variants)
    {
        this.variants = variants;
        this.type = type;
    }

    public Transporter()
    {

    }

    public void Serialize(string inifile)
    {
        INIParser ini = new INIParser();
        ini.Open(inifile);
        ini.WriteValue("Transporter", "type", type.ToString());

        for (int i = 0; i < variants.Count; i++)
        {
            ini.WriteValue("Transporter", "variant"+i, variants[i].name);
        }
    }

    /*
    public static Transporter Deserialize(string inifile)
    {
        Transporter t = new Transporter();
        INIParser ini = new INIParser();
        ini.Open(inifile);

        t.type = (Transporter.Type)Enum.Parse(typeof(Transporter.Type), ini.ReadValue("Transporter", "type", "FAIL"));

        List<GameObject> vList = new List<GameObject>();

        string variant = "";
        int i = 0;

        while(variant != "FAIL"){
            variant = ini.ReadValue("Transporter", "variant"+i, "FAIL");

            //here we need to find each gameobject by name

            i++;
        }
    }
    */
}
