﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationObject {

    //EG: core_waterbottle
    public string stringName = "";

    //EG: eng, Water Bottle
    public Dictionary<string, string> languageMap = new Dictionary<string, string>();

    public LocalizationObject(string stringName, Dictionary<string, string> languageMap)
    {
        this.stringName = stringName;
        this.languageMap = languageMap;
    }

    public void SetLanguageMap(Dictionary<string, string> newLanMap)
    {
        languageMap = newLanMap;
        
    }
}
