﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MFAddon {

    public string addonName;
    public string addonID;
    public string addonDescription;
    public string addonVersion;

    public MFAddon()
    {

    }

    public MFAddon(string addonName, string addonID, string addonDescription, string addonVersion)
    {
        this.addonName = addonName;
        this.addonID = addonID;
        this.addonDescription = addonDescription;
        this.addonVersion = addonVersion;
    }
}
