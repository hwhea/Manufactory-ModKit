﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;



public class WBuildConfig : EditorWindow {

    static string entityName = "";
    static string entityID = "";
    static string entityDesc = "";

    static float marketPrice = 0f;

    static bool confirmSS;
    static Texture2D image;

    static Object entityGameObject;
    public static WBuildConfig window;

    INIParser ini;

    static bool checkForLocObExists = false;
    static bool locObForNameExists = false;
    static bool locObForDescExists = false;
    static int lastLocObCheck = 999;
    static int checkEvery = 10;

    static ManufactoryEntity.EntityType buildType;
    static string buildTypeAsString;

    static bool hasBuiltEntity = false;

    /*
     * Non-item
    */
    static bool isPartOfGroup = false;
    static string entityGroupName = "";

    /*
     *  TRANSPORTER SPECIFIC  
    */
    public static Transporter transporter;
    static int transporterVariantCount = 0;

    //-----------------------

	static void Init()
    {
        window = (WBuildConfig)EditorWindow.GetWindow(typeof(WBuildConfig));
        window.Show();
    }

    public static void ForceLocObsCheck()
    {
        checkForLocObExists = true;
    }

    private void OnGUI()
    {
        if (AddonCore.HasBeenSetup())
        {
            GUILayout.Label("Entity Configuration");

            GUILayout.Space(10f);
            buildType = (ManufactoryEntity.EntityType)EditorGUILayout.EnumPopup(buildType);
            buildTypeAsString = buildType.ToString();
            GUILayout.Space(10f);

            string oldEntityID = entityID;
            string oldEntityName = entityName;
            string oldEntityDesc = entityDesc;

            entityID = EditorGUILayout.TextField(buildTypeAsString + " ID", entityID);
            
            if(oldEntityID != entityID)
            {
                entityName = entityID + "_name";
                entityDesc = entityID + "_desc";
            }

            entityName = EditorGUILayout.TextField(buildTypeAsString + " Name String", entityName);
            entityDesc = EditorGUILayout.TextField(buildTypeAsString + " Description String", entityDesc);

            if (oldEntityID != entityID || oldEntityName != entityName || oldEntityDesc != entityDesc)
            {
                checkForLocObExists = true;
            }

            if(checkForLocObExists)
            {
                Debug.Log("Checking for loc ob");
                locObForNameExists = LocalizationManager.FindLocalizationObjectByName(entityName) != null;
                locObForDescExists = LocalizationManager.FindLocalizationObjectByName(entityDesc) != null;
                checkForLocObExists = false;
            }
        

            if(!locObForNameExists)
            {
                EditorGUILayout.LabelField("Your entity name wasn't found in the strings database.", MMKStyle.TextColor(Color.red));

                if(GUILayout.Button("Create Name String"))
                {
                    WAddString window = (WAddString)EditorWindow.GetWindow(typeof(WAddString), false, "Strings");
                    WAddString.PopulateNewString(entityName);
                }

            }
            else if (!locObForDescExists)
            {
                EditorGUILayout.LabelField("Entity string found.", MMKStyle.TextColor(Color.green));
                EditorGUILayout.LabelField("Your entity description wasn't found in the strings database.", MMKStyle.TextColor(Color.red));

                if (GUILayout.Button("Create Description String"))
                {
                    WAddString window = (WAddString)EditorWindow.GetWindow(typeof(WAddString), false, "Strings");
                    WAddString.PopulateNewString(entityDesc);
                }
            }
            else
            {
                marketPrice = EditorGUILayout.FloatField("Market Price", marketPrice);
                entityGameObject = EditorGUILayout.ObjectField("Prefab to search for", entityGameObject, typeof(GameObject), false) as GameObject;
            }

            
            
            if (entityGameObject != null)
            {

                if (image == null)
                {
                    if (GUILayout.Button("Generate Screenshot"))
                    {
                        WThumbnailGenerator tg = (WThumbnailGenerator)EditorWindow.GetWindow(typeof(WThumbnailGenerator), false, "Thumbnail Generator");
                        tg.Setup(entityID, entityGameObject);
                    }

                }
                else
                {

                    GUILayout.Label("Screenshot taken!", MMKStyle.TextColor(Color.green));


                    //THIS IS WHERE THINGS CHANGE DEPENDING ON ITEM TYPE.
                    if(buildType == ManufactoryEntity.EntityType.Machine)
                    {
                        GUILayout.Space(20f);
                        GUILayout.Label("Please define any parameters for this new machine:");
                        isPartOfGroup = EditorGUILayout.Toggle("Entity is part of a group", isPartOfGroup);

                        if (isPartOfGroup)
                        {
                            entityGroupName = EditorGUILayout.TextField("Group Name:", entityGroupName);
                        }
                    }

                    
                    if (GUILayout.Button("Build Entity"))
                    {
                        bool canRun = true;

                        AddonCore.CheckAddonPaths();

                        if (entityID.Length < 3)
                        {
                            EditorUtility.DisplayDialog("Error!", "EntityID is too short", "Whoops...");
                            canRun = false;
                        }

                        if (entityName.Length < 2)
                        {
                            EditorUtility.DisplayDialog("Error!", "Entity name is too short", "Whoops...");
                            canRun = false;
                        }

                        if (entityDesc.Length < 5)
                        {
                            EditorUtility.DisplayDialog("Error!", "Entity description is too short", "Whoops...");
                            canRun = false;
                        }

                        if(entityDesc.Length > 150)
                        {
                            EditorUtility.DisplayDialog("Error!", "Entity description is too long. You can enter upto 150 characters.", "Whoops...");
                            canRun = false;
                        }

                        if (entityID.Contains(" "))
                        {
                            canRun = false;
                            EditorUtility.DisplayDialog("Error!", "Entity ID cannot contain spaces. Use underscores.", "Whoops...");
                        }

                        if (canRun)
                        {
                            entityGameObject.name = entityID;
                            

                            Object parentObject = PrefabUtility.GetPrefabObject(entityGameObject);
                            string path = AssetDatabase.GetAssetPath(parentObject);
                            string gameObjectName = entityGameObject.name;
                            AssetDatabase.RenameAsset(path, gameObjectName);
                            path = AssetDatabase.GetAssetPath(parentObject);

                            ini = new INIParser();
                            bool exists = !AssetManager.EntityExists(entityID); 

                            if (exists)
                            {

                                //Create core entity config file.
                                ManufactoryEntity me = new ManufactoryEntity(entityID, entityName, entityDesc, marketPrice, path, buildType);
                                AssetManager.AddEntity(me);

                                //Refresh and alert
                                AssetDatabase.Refresh();
                                EditorUtility.DisplayDialog("Success!", "Your entity's config was generated.", "Thanks!");
                                AssetManager.RefreshEntityList();

                                //ALL ENTITIES
                                ini = new INIParser();
                                ini.Open(AddonCore.AddonConfigsDirectory + me.entityID + ".mfdata.txt");
                                ini.WriteValue("Entity", "price", marketPrice);
                                ini.WriteValue("Entity", "gameObjectPath", path);
                                ini.WriteValue("Entity", "gameObjectName", gameObjectName);
                                ini.Close();

                                //handling for building different entities
                                if (buildType == ManufactoryEntity.EntityType.Item)
                                {
                                    //Additional fields required for this entity type.
                                    
                                }
                                else if (buildType == ManufactoryEntity.EntityType.Machine) //if it's a transporter
                                {
                                    ini = new INIParser();
                                    ini.Open(AddonCore.AddonConfigsDirectory + me.entityID + ".mfdata.txt");

                                    if (isPartOfGroup)
                                    {
                                        ini.WriteValue("Group", "groupName", entityGroupName);
                                        Debug.Log("Part of group " + entityGroupName);
                                    }

                                    ini.WriteValue("Machine", "isMachine", "True");

                                    ini.Close();

                                }

                                WAddonManager.window.Focus();
                                ClearAndResetWindow();
                            }
                            else
                            {
                                EditorUtility.DisplayDialog("Error!", "This entity could not be added as it already exists.", "Whoops...");
                            }
                        }
                    }
                    
                }

                
                
            }
            else
            {
                GUILayout.Label("Add a GameObject to finish creation...");
            }
        }
        else
        {
            GUILayout.Label("You must set up your addon first.");
        }
        


    }

    void OnEnable()
    {
        Init();   
        transporter = new Transporter();
    }

    static void ClearAndResetWindow()
    {
        entityName = "";
        entityDesc = "";
        entityID = "";
        entityGameObject = null;
        image = null;
        marketPrice = 0;
        window.Close();
    }

    public static void SetImage(Texture2D newimg)
    {
        image = newimg;
    }
}

#endif