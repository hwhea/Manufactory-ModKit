﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


public class WAddString : EditorWindow {

    int stringIdxSelected = 0;
    string filterTerm = "";

    static LocalizationObject currentLocOb;

    static string newStringName = "";
    static string newStringLang = "";
    static string newStringDesc = "";

    static bool modificationValid = true;
    static bool closeOnCreate = false;
    bool changesMade = false; //for saving
    static int iterCount = 55;

    static void Init()
    {
        WAddString window = (WAddString)EditorWindow.GetWindow(typeof(WAddString), false, "Strings");
        window.minSize = new Vector2(300, 400);
        RefreshLocObs();
    }

    public static void PopulateNewString(string nom)
    {
        newStringName = nom;
        closeOnCreate = true;
    }

    private void OnGUI()
    {
        if (LocalizationManager.locObs == null)
            RefreshLocObs();

        GUILayout.Label("All language codes use the ISO 639-1 standard.", EditorStyles.helpBox);

        GUILayout.Space(20f);

        string dropDownPromptString = "Create New String";

        filterTerm = EditorGUILayout.TextField("Filter List: ", filterTerm);
        string[] loNames = new string[LocalizationManager.locObs.Length+1];

        loNames[0] = dropDownPromptString;

        for (int i = 1; i <LocalizationManager.locObs.Length+1; i++)
        {
            if(LocalizationManager.locObs[i-1].stringName.Contains(filterTerm))
                loNames[i] =LocalizationManager.locObs[i-1].stringName;
        }

        int oldStringIdxSelected = stringIdxSelected;
        stringIdxSelected = EditorGUILayout.Popup(stringIdxSelected, loNames);

        if(stringIdxSelected != oldStringIdxSelected)
        {
            currentLocOb = LocalizationManager.FindLocalizationObjectByName(loNames[stringIdxSelected]);
        }

        if(stringIdxSelected == 0) //create new string
        {
            GUILayout.Space(20f);
            EditorGUILayout.LabelField("Create New String");

            newStringName = EditorGUILayout.TextField("String Name: ", newStringName);

            if(newStringName.Length > 4 && !newStringName.Contains(" "))
            {

                newStringLang = EditorGUILayout.TextField("ISO Code: ", newStringLang);
                newStringDesc = EditorGUILayout.TextField("Translation: ", newStringDesc);

                if(newStringLang.Length == 2 && !newStringLang.Contains(" "))
                {
                    if (GUILayout.Button("Create"))
                    {
                        Dictionary<string, string> newStringDict = new Dictionary<string, string>();
                        newStringDict.Add(newStringLang, newStringDesc);
                        LocalizationManager.SaveLocObj(newStringName, newStringDict);
                        RefreshLocObs();
                        WBuildConfig.ForceLocObsCheck(); 

                        newStringDesc = "";
                        newStringLang = "";
                        newStringName = "";

                        if (closeOnCreate)
                        {
                            closeOnCreate = false;
                            WBuildConfig.window.Focus();
                            this.Close();
                        }
                        else
                        {
                            stringIdxSelected =LocalizationManager.locObs.Length;
                        }

                    }
                }
                else
                {
                    EditorGUILayout.LabelField("Language ISO codes are 2 characters.", EditorStyles.helpBox);
                }

                
            }
            else if(newStringName.Length > 0)
            {
                EditorGUILayout.LabelField("String names must be > 4 characters and should not contain spaces.", EditorStyles.helpBox);
            }
        }
        else //modify existing
        {
            GUILayout.Space(20f);
            modificationValid = true; //reset 
      
            if(iterCount > 5)
            {
                
                iterCount = 0;
            }
            else
            {
                iterCount++;
            }
            

            if (currentLocOb == null)
                stringIdxSelected = 0;

            if (currentLocOb.languageMap.Keys == null)
                stringIdxSelected = 0;

            //This will store <<oldkey, newkey>, value>
            Dictionary<string, string> translationsNew = new Dictionary<string, string>();

            List<string> keys = new List<string>(currentLocOb.languageMap.Keys);

            

            foreach (string key in keys)
            {
                string newKey = key;
                string cloVal = currentLocOb.languageMap[key];


                EditorGUILayout.BeginHorizontal();
                newKey = EditorGUILayout.TextField("ISO Code: ", key);
                if (GUILayout.Button("X"))
                {
                    currentLocOb.languageMap.Remove(key);
                    LocalizationManager.SaveLocObj(currentLocOb.stringName, currentLocOb.languageMap);
                }
                EditorGUILayout.EndHorizontal();
                cloVal = EditorGUILayout.TextField("Translation: ", cloVal);
                

                if(newKey.Length != 2)
                {
                    Debug.LogWarning("Invalid key length!");
                    modificationValid = false;
                }

                //store the edited text fields and if necessary, log key changes.
                translationsNew.Add(newKey, cloVal);

                if (newKey != key || cloVal != currentLocOb.languageMap[key])
                    changesMade = true;
            }




            //ini delete section if changed, 

            bool istrue = GUILayout.Button("New");

            if (istrue)
            {
                translationsNew.Add("", "");
                Debug.Log(currentLocOb.languageMap.Count);
                changesMade = true;
            }

            if (modificationValid)
            {
                if (changesMade)
                {
                    GUILayout.Space(20f);
                    EditorGUILayout.LabelField("You have made changes to this string. Remember to save them.", MMKStyle.TextColor(Color.cyan));

                    currentLocOb.languageMap = translationsNew;


                    if (GUILayout.Button("Save changes", GUILayout.MinHeight(20f)))
                    {

                        LocalizationManager.SaveLocObj(currentLocOb.stringName, translationsNew);
                        changesMade = false;
                    }

                }
            }
            else
            {
                GUILayout.Label("Your country identifier (ISO Code) must be 2 characters.", MMKStyle.TextColor(Color.red));
            }
            

            if (GUILayout.Button("Delete this Localization"))
            {
                LocalizationManager.DeleteLocObj(currentLocOb.stringName);
                RefreshLocObs();
                stringIdxSelected = 0;
            }
        }



    }

    private static void RefreshLocObs()
    {
       LocalizationManager.locObs = ConfigManager.GetAllLocalizationObjects();
    }




}
