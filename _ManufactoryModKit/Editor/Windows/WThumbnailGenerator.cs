﻿
#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class WThumbnailGenerator : EditorWindow {

    string itemID;
    string relativeTempFilePath = "/_ManufactoryModKit/tempdata/temp.png";
    string tempFilePath = "";

    int imgWidth = 128;
    int imgHeight = 128;
    byte[] imageRawData = null;

    GameObject currentlyInScene = null;
    Vector3 imageTranslate = Vector3.zero;
    Vector3 imageRotation = Vector3.zero;

    int previewRefreshRate = 15;
    int lastRefresh = 999;

    Camera ssCam;

    public void Init()
    {
        WThumbnailGenerator window = (WThumbnailGenerator)EditorWindow.GetWindow(typeof(WThumbnailGenerator), false, "Thumbnail Generator");
        window.maxSize = new Vector2(400, 400);
    }

    void OnEnable() //sort of becomes our constructor
    {
        this.maxSize = new Vector2(400, 400);
        ssCam = GameObject.Find("SSCamera").GetComponent<Camera>();
        tempFilePath = Application.dataPath + relativeTempFilePath;
    }

    void OnDisable()
    {
        DestroyImmediate(currentlyInScene);
        imageTranslate = Vector3.zero;
        imageRotation = Vector3.zero;
    }

    void OnGUI()
    {
        if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "stage")
        {
            EditorSceneManager.OpenScene(Application.dataPath+"/_ManufactoryModKit/Scenes/stage.unity");
            GUILayout.Label("You're not in the stage scene, moving you!");
            this.Close();
            return;
        }

        if (ssCam == null)
            ssCam = GameObject.Find("SSCamera").GetComponent<Camera>();

        if(currentlyInScene != null)
        {
            currentlyInScene.transform.position = imageTranslate;
            currentlyInScene.transform.rotation = Quaternion.Euler(imageRotation);

            Vector3 oldImageTranslate = imageTranslate;
            Vector3 oldImageRotation = imageRotation;

            imageTranslate = EditorGUILayout.Vector3Field("Entity Position", imageTranslate);
            imageRotation = EditorGUILayout.Vector3Field("Entity Rotation", imageRotation);

            
            

            if((lastRefresh > previewRefreshRate && (oldImageRotation != imageRotation || oldImageTranslate != imageTranslate)) || lastRefresh == 999)
            {

                RenderTexture rt = new RenderTexture(imgWidth, imgHeight, 24);
                ssCam.targetTexture = rt;

                TextureFormat tFormat = TextureFormat.ARGB32;
           
                Texture2D screenShot = new Texture2D(imgWidth, imgHeight, tFormat, false);
                ssCam.Render();
                RenderTexture.active = rt;
                screenShot.ReadPixels(new Rect(0, 0, imgWidth, imgHeight), 0, 0);
                ssCam.targetTexture = null;
                RenderTexture.active = null;
                imageRawData = screenShot.EncodeToPNG();
                
                System.IO.File.WriteAllBytes(tempFilePath, imageRawData);
                Debug.Log("Preview updated!");
                lastRefresh = 0;
            }
            else
            {
                lastRefresh++;
            }


            Texture2D texture = new Texture2D(imgWidth, imgHeight);
            texture.LoadImage(imageRawData);
            texture.alphaIsTransparency = true;
            EditorGUI.DrawPreviewTexture(new Rect(10, 120, imgWidth, imgHeight), texture);
            

            if (GUILayout.Button("Capture", GUILayout.MinHeight(40f)))
            {
                File.Copy(tempFilePath, AddonCore.AddonThumbsDirectory + itemID + "_thumb.png", true);
                WBuildConfig.SetImage(texture);
                WBuildConfig.window.Focus();
                this.Close();
            }

        }
        else
        {
            GUILayout.Label("It looks like there was an error...");
        }
    }

    public void Setup(string itemID, Object go)
    {
        this.itemID = itemID;
        currentlyInScene = (GameObject)GameObject.Instantiate(go, null);
    }


}

#endif