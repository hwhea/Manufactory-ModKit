﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class WAddonManager : EditorWindow {

    public static WAddonManager window;

    Dictionary<string, bool> itemListVisible = new Dictionary<string, bool>();
    Dictionary<string, bool> transporterListVisible = new Dictionary<string, bool>();
    Dictionary<string, bool> machineListVisible = new Dictionary<string, bool>();
    bool itemsFoldout = false;

    //For setting up a new addon...
    string nAddonID = "";
    string nAddonName = "";
    string nAddonDesc = "";
    string nAddonVersion = "v1.0";

    bool editingSettings = false;
    bool editFirstRun = true;
    //----------

    //Windows
    WBuildConfig wBC = new WBuildConfig();
    WAddString wAS = new WAddString();
    
    private void ShowBuildWindow()
    {
        if(wBC == null)
        {
            wBC = new WBuildConfig();
        }
        wBC.Show();
    }

    private void ShowStringsWindow()
    {
        if (wAS == null)
        {
            wAS = new WAddString();
        }
        wAS.Show();
    }

    [MenuItem("Manufactory/AddonManager")]
	static void Init()
    {
        window = (WAddonManager)EditorWindow.GetWindow(typeof(WAddonManager), false, "MMK Dashboard");
        window.maxSize = new Vector2(500, 700);
        window.minSize = new Vector2(500, 500);
        window.Show();
        AssetManager.RefreshEntityList();
    }

    private Texture2D MakeTex(int width, int height, Color col)
    {
        Color[] pix = new Color[width * height];

        for (int i = 0; i < pix.Length; i++)
            pix[i] = col;

        Texture2D result = new Texture2D(width, height);
        result.SetPixels(pix);
        result.Apply();

        return result;
    }

    GUIStyle GetBackgroundColor(Color col)
    {
        GUIStyle style = new GUIStyle();
        style.normal.background = MakeTex(500, 200, col);
        return style;
    }

    GUIStyle GetFont(int size)
    {
        GUIStyle style = new GUIStyle();
        style.fontSize = size;
        return style;
    }

    GUILayoutOption ButtonStyle()
    {
        return GUILayout.MinWidth(170f) ;
    }

    private void OnGUI()
    {
        GUILayout.BeginHorizontal(GetBackgroundColor(Color.blue), GUILayout.MinHeight(30f));
        GUILayout.FlexibleSpace();
        GUILayout.Label("Manufactory Mod Kit", GetFont(20));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal(GetBackgroundColor(Color.white), GUILayout.MinHeight(10f));
        GUILayout.FlexibleSpace();
        GUILayout.Label("Currently Editing: " + AddonCore.ThisAddon.addonName, GetFont(10));
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        if (AddonCore.HasBeenSetup() && !editingSettings){
            GUILayout.BeginHorizontal();

            GUILayout.BeginVertical(); //Left side
            itemsFoldout = EditorGUILayout.Foldout(itemsFoldout, "Items");

            if (itemsFoldout)
            {
                EditorGUI.indentLevel = 2;
                foreach (ManufactoryEntity me in AssetManager.entityList)
                {
                    if (!itemListVisible.ContainsKey(me.entityID))
                        itemListVisible.Add(me.entityID, false);

                    ManufactoryEntity oldMa = new ManufactoryEntity(me);

                    itemListVisible[me.entityID] = EditorGUILayout.Foldout(itemListVisible[me.entityID], me.entityID);
                    if (itemListVisible[me.entityID])
                    {
                        GUILayout.BeginVertical(EditorStyles.helpBox);
                        GUILayout.BeginHorizontal();
                        me.entityID = EditorGUILayout.TextField("", me.entityID, GUILayout.MinWidth(0));
                        me.entityName = EditorGUILayout.TextField("", me.entityName, GUILayout.MinWidth(0));
                        GUILayout.EndHorizontal();
                        GUILayout.BeginHorizontal();
                        me.entityDesc = EditorGUILayout.TextField("", me.entityDesc, GUILayout.MinWidth(0));
                        if (GUILayout.Button("X"))
                        {
                            ConfigManager.DeleteEntity(me);
                            AssetManager.RefreshEntityList();
                        }
                        GUILayout.EndHorizontal();
                        GUILayout.EndVertical();

                        if (oldMa != me)
                        {
                            ConfigManager.WriteEntity(me);
                        }
                    }

                }
                EditorGUI.indentLevel = 1;
            }
            GUILayout.EndVertical();

            GUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.MinHeight(100f), GUILayout.MaxWidth(200f));
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Manage");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(GUILayout.Button("Add Entity", ButtonStyle()))
            {
                ShowBuildWindow();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(GUILayout.Button("Manage Strings", ButtonStyle()))
            {
                ShowStringsWindow();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(20f);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("This Entity");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Button("Edit Entity", ButtonStyle());
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Button("Destroy Entity", ButtonStyle());
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Button("Retake Thumbnail", ButtonStyle());
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.Space(20f);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("Addon Settings");
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(GUILayout.Button("Edit Addon", ButtonStyle()))
            {
                editingSettings = true;
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if(GUILayout.Button("Build Addon", ButtonStyle(), GUILayout.MinHeight(40f)))
            {
                if (AssetManager.BuildAssetBundle())
                {
                    EditorUtility.DisplayDialog("Success!", "Addon built!", "Whoops...");
                }
                else
                {
                    EditorUtility.DisplayDialog("Error!", "Failed to build asset bundle. Check error log.", "OK");
                }
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.EndVertical();

            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginVertical(GUILayout.MaxWidth(300f));

            string introLabelString = "Looks like you need to set up a new addon...";
            string submitButtonString = "Create Project";
            string allGoodString = " Everything looks good! Let's get this show on the road.";

            if (editingSettings && editFirstRun)
            {
                nAddonID = AddonCore.ThisAddon.addonID;
                nAddonName = AddonCore.ThisAddon.addonName;
                nAddonDesc = AddonCore.ThisAddon.addonDescription;
                nAddonVersion = AddonCore.ThisAddon.addonVersion;

                
                editFirstRun = false;
            }

            if (editingSettings)
            {
                introLabelString = "Edit your Addon Settings.";
                submitButtonString = "Save Changes";
                allGoodString = "All of your changes are valid, happy modding! :)";
            }

            GUILayout.Label(introLabelString);

            nAddonID = EditorGUILayout.TextField("Addon ID", nAddonID);
            nAddonName = EditorGUILayout.TextField("Addon Name", nAddonName);
            nAddonDesc = EditorGUILayout.TextField("Addon Description", nAddonDesc);
            nAddonVersion = EditorGUILayout.TextField("Addon Version", nAddonVersion);

            bool canCreate = true;

            if(nAddonID.Contains(" "))
            {
                GUILayout.Label("Your Addon ID cannot contain spaces.", MMKStyle.TextColor(Color.red));
                canCreate = false;
            }

            if (nAddonVersion.Contains(" "))
            {
                GUILayout.Label("Your Addon version cannot contain spaces.", MMKStyle.TextColor(Color.red));
                canCreate = false;
            }

            if (nAddonID.Contains("!") || nAddonID.Contains("@") || nAddonID.Contains("\\")){
                GUILayout.Label("Your Addon ID contains invalid characters.", MMKStyle.TextColor(Color.red));
                canCreate = false;
            }

            if(nAddonID.Length <= 3 || nAddonName.Length <= 4 || nAddonDesc.Length <= 10 || nAddonVersion.Length == 0)
            {
                GUILayout.Label("One or more of your entries is too short.", MMKStyle.TextColor(Color.red));
                canCreate = false;
            }

            if (canCreate)
            {
                GUILayout.Label(allGoodString, MMKStyle.TextColor(Color.green));

                AddonCore.CheckAddonPaths();

                if (GUILayout.Button(submitButtonString, GUILayout.MinHeight(40f)))
                {
                    ConfigManager.WriteAddon(new MFAddon(nAddonName, nAddonID, nAddonDesc, nAddonVersion));
                    AssetManager.RefreshEntityList();
                    editingSettings = false;
                    editFirstRun = true;
                }
            }

            GUILayout.EndVertical();
        }
    }

}
