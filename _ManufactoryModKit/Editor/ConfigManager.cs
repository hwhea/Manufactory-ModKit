﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

public class ConfigManager : MonoBehaviour {

    public static List<ManufactoryEntity> DiscoverAllEntities()
    {
        INIParser ini = new INIParser();

        List<ManufactoryEntity> melist = new List<ManufactoryEntity>();

        string path = Application.dataPath;
        string[] fileList = Directory.GetFiles(AddonCore.AddonConfigsDirectory, "*.mfdata.txt", SearchOption.AllDirectories);

        foreach (string filePath in fileList)
        {
            ManufactoryEntity me = new ManufactoryEntity();
            ini = new INIParser();
            ini.Open(filePath);
            me.entityID = ini.ReadValue("Info", "entityID", "ERROR");
            me.entityName = ini.ReadValue("Info", "entityName", "ERROR");
            me.entityDesc = ini.ReadValue("Info", "entityDesc", "ERROR");
            me.thumbnail = ini.ReadValue("Info", "thumbnail", "ERROR");
            float.TryParse(ini.ReadValue("Entity", "price", "ERROR"), out me.price);
            me.gameObjectPath = ini.ReadValue("Entity", "gameObjectPath", "ERROR");
            ini.Close();
            melist.Add(me);
        }

        return melist;
    }

    public static void WriteAddon(MFAddon mfa)
    {
        if (mfa.addonID.Length < 3 || mfa.addonName.Length < 3)
        {
            EditorUtility.DisplayDialog("Error!", "Addon ID or name is too short.", "Thanks!");
        }
        else
        {
            INIParser ini = new INIParser();
            ini.Open(AddonCore.AddonMasterConfigPath);
            ini.WriteValue("Package", "addonID", mfa.addonID);
            ini.WriteValue("Package", "addonName", mfa.addonName);
            ini.WriteValue("Package", "addonVersion", mfa.addonVersion);
            ini.WriteValue("Package", "addonDescription", mfa.addonDescription);
            ini.Close();
        }
    }

    public static MFAddon ReadAddon()
    {
        INIParser ini = new INIParser();
        ini.Open(AddonCore.AddonMasterConfigPath);

        MFAddon mfa = new MFAddon();

        mfa.addonName = ini.ReadValue("Package", "addonName", "FAIL");
        mfa.addonDescription = ini.ReadValue("Package", "addonDescription", "FAIL");
        mfa.addonID = ini.ReadValue("Package", "addonID", "FAIL");
        mfa.addonVersion = ini.ReadValue("Package", "addonVersion", "FAIL");

        ini.Close();

        return mfa;
    }


    public static void DeleteEntity(ManufactoryEntity me)
    {
        File.Delete(AddonCore.AddonConfigsDirectory + me.entityID + ".mfdata.txt");
        File.Delete(AddonCore.AddonThumbsDirectory + me.thumbnail);
    }

    public static void WriteEntity(ManufactoryEntity me)
    {
        INIParser ini = new INIParser();
        ini.Open(AddonCore.AddonConfigsDirectory + me.entityID + ".mfdata.txt");
        ini.WriteValue("Info", "entityID", me.entityID);
        ini.WriteValue("Info", "entityName", me.entityName);
        ini.WriteValue("Info", "entityDesc", me.entityDesc);
        ini.WriteValue("Info", "thumbnail", me.entityID + "_thumb.png");
        ini.WriteValue("Info", "entityType", me.entityType.ToString());
        ini.Close();
    }


    

    public static void WriteLocalizationObject(string name, Dictionary<string, string> newLocs)
    {
        INIParser ini = new INIParser();
        ini.Open(AddonCore.AddonLocalizationFilePath);

        if (ini.IsSectionExists(name))
        {
            ini.SectionDelete(name);
        }


        foreach (KeyValuePair<string, string> kvp in newLocs)
        {
            Debug.Log("writing " + kvp.Key + " / " + kvp.Value + " in " + name);
            ini.WriteValue(name, kvp.Key, kvp.Value);
        }

        ini.Close();
    }

    public static void DeleteLocalizationObject(string name)
    {
        INIParser ini = new INIParser();
        ini.Open(AddonCore.AddonLocalizationFilePath);
        ini.SectionDelete(name);
        ini.Close();
    }

    public static LocalizationObject[] GetAllLocalizationObjects()
    {
        INIParser ini = new INIParser();
        ini.Open(AddonCore.AddonLocalizationFilePath);

        List<LocalizationObject> lo = new List<LocalizationObject>();

        foreach (KeyValuePair<string, Dictionary<string, string>> section in ini.GetAllSections())
        {
            string title;

            Dictionary<string, string> translations = new Dictionary<string, string>();

            title = section.Key;
            translations = section.Value;


            lo.Add(new LocalizationObject(title, translations));
        }

        ini.Close();

        return lo.ToArray();

    }
}
