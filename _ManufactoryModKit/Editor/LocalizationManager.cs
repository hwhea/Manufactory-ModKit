﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationManager {

    public static LocalizationObject[] locObs;

    public static LocalizationObject FindLocalizationObjectByName(string term)
    {
        locObs = ConfigManager.GetAllLocalizationObjects();
        Debug.Log("LOCOBS LENGHT" + locObs.Length);
        foreach (LocalizationObject lo in locObs)
        {
            if (lo.stringName == term)
            {
                return lo;
            }
        }

        return null;
    }

    public static bool SaveLocObj(string name, Dictionary<string, string> newLocs)
    {

        LocalizationObject loiq = FindLocalizationObjectByName(name);

        if (loiq != null)
            loiq.SetLanguageMap(newLocs);

        ConfigManager.WriteLocalizationObject(name, newLocs);

        locObs = ConfigManager.GetAllLocalizationObjects();
        return true;
    }

    public static void DeleteLocObj(string name)
    {
        ConfigManager.DeleteLocalizationObject(name);
        locObs = ConfigManager.GetAllLocalizationObjects();
    }
}
