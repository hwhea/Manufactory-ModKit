﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class AssetManager : MonoBehaviour {

    public static List<ManufactoryEntity> entityList = new List<ManufactoryEntity>();
    static AssetBundleBuild[] buildMap = new AssetBundleBuild[1];


    public static void AddEntity(ManufactoryEntity me)
    {
        ConfigManager.WriteEntity(me);
        entityList.Add(me);
        RefreshEntityList();
    }

    public static void RemoveEntity(ManufactoryEntity me)
    {
        ConfigManager.DeleteEntity(me);
        entityList.Remove(me);
        RefreshEntityList();
    }

    public static bool EntityExists(string id)
    {
        RefreshEntityList();
        foreach (ManufactoryEntity ma in entityList)
        {
            if (id == ma.entityID)
            {
                return true;
            }
        }
        return false;
    }

    public static void RefreshEntityList()
    {
        entityList.Clear();

        entityList = ConfigManager.DiscoverAllEntities();

    }

    public static bool BuildAssetBundle()
    {
        ApplyAssetBundleSettings();
        try
        {
            foreach(string sn in buildMap[0].assetNames)
            {
                Debug.Log(sn);
            }

            Debug.Log(buildMap[0].assetBundleName);

            return BuildPipeline.BuildAssetBundles("Assets/Output", buildMap, BuildAssetBundleOptions.ForceRebuildAssetBundle, BuildTarget.StandaloneWindows);
        }
        catch (System.Exception e)
        {
            Debug.LogError("Full BuildMap readout: " + buildMap.ToString());
            return false;
        }
    }

    private static void ApplyAssetBundleSettings()
    {

        RefreshEntityList();

        if (buildMap[0].assetBundleName != AddonCore.ThisAddon.addonID + ".mfaddon")
        {
            buildMap[0].assetBundleName = AddonCore.ThisAddon.addonID + ".mfaddon";
        }

        List<string> entitiesToAdd = new List<string>();

        foreach (ManufactoryEntity me in entityList)
        {
            entitiesToAdd.Add(me.gameObjectPath);
            Debug.Log(me.thumbnail + " <<<<<<<< GO Path");
            entitiesToAdd.Add("Assets/" + AddonCore.addonThumbsFolder + "/" + me.thumbnail);
            entitiesToAdd.Add("Assets/" + AddonCore.addonConfigsFolder + "/" + me.entityID + ".mfdata.txt");
        }

        entitiesToAdd.Add("Assets/" + AddonCore.addonCoreFolder + "/manufactory.addon.txt");
        entitiesToAdd.Add("Assets/"+AddonCore.addonCoreFolder+"/localization.txt");

        buildMap[0].assetNames = entitiesToAdd.ToArray();
    }
}
