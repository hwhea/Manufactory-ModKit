﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AddonCore : MonoBehaviour {

    public static string addonCoreFolder = "AddonCore";
    public static string addonThumbsFolder = "Thumbnails";
    public static string addonPrefabsFolder = "Prefabs";
    public static string addonConfigsFolder = "Configs";

    private static MFAddon thisAddon;


    static string addonLocalizationFilePath;
    static string addonMasterConfigPath;
    static string addonThumbsDirectory;
    static string addonPrefabsDirectory;
    static string addonConfigsDirectory;

    public static string AddonMasterConfigPath
    {
        get
        {
            return Application.dataPath + "/" + addonCoreFolder + "/manufactory.addon.txt";
        }

        set
        {
            addonMasterConfigPath = value;
        }
    }

    public static string AddonThumbsDirectory
    {
        get
        {
            return Application.dataPath + "/" + addonThumbsFolder + "/";
        }

        set
        {
            addonThumbsDirectory = value;
        }
    }

    public static string AddonPrefabsDirectory
    {
        get
        {
            return Application.dataPath + "/" + addonPrefabsDirectory + "/";
        }

        set
        {
            addonPrefabsDirectory = value;
        }
    }

    public static string AddonConfigsDirectory
    {
        get
        {
            return Application.dataPath + "/" + addonConfigsFolder + "/";
        }

        set
        {
            addonConfigsDirectory = value;
        }
    }

    public static MFAddon ThisAddon
    {
        get
        {
            if (HasBeenSetup())
            {
                if(thisAddon == null)
                {
                    return ConfigManager.ReadAddon();
                }
                else
                {
                    return thisAddon;
                }
                
            }
            else
            {
                return null;
            }
        }

        set
        {
            thisAddon = value;
        }
    }

    public static string AddonLocalizationFilePath
    {
        get
        {
            return Application.dataPath + "/" + addonCoreFolder + "/localization.txt";
        }

        set
        {
            addonLocalizationFilePath = value;
        }
    }

    static void CreateDirectoryIfNotExists(string dir)
    {
        if (!Directory.Exists(dir))
            Directory.CreateDirectory(dir);
    }

    public static void CheckAddonPaths()
    {
        CreateDirectoryIfNotExists(AddonPrefabsDirectory);
        CreateDirectoryIfNotExists(AddonThumbsDirectory);
        CreateDirectoryIfNotExists(AddonConfigsDirectory);
    }

    public static bool HasBeenSetup()
    {
        return (System.IO.File.Exists(AddonMasterConfigPath));
    }
}
